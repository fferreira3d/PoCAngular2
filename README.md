# Angular 2 - Prova de Conceito
###### Curso de Arquitetura de Software Distribuído - Oferta 5
###### Disciplina de Produtividade no Desenvolvimento de Software

----

#### Descrição da tecnologia
Angular 2 não é um update da versão 1, e sim um novo framework, de forma que é necessario diversas adaptações em um app desenvolvido na versão 1 migrar para a versão 2.
Angular 2 traz novas caracteristicas, como maior suporte a dispositivos mobiles e os recursos mais modernos dos browsers,
alem de recomendar o desenvolvimento utilizando TypeScript (linguagem desenvolvida pela Microsoft e baseada em JavaScript e suporta funcionalidades no estilo orientado a objetos).
Devido a estas caracteristicas a curva de aprendizado torna se mais lenta.
Uma aplicação em Angular 2 tem como requisito a intalação do *Node.js* v5.x.x e do *npm* v3.x.x ou superiores.

Angular 2 foi projetado para desenvolvimento mobile a partir do zero. Além de poder de processamento limitado, os dispositivos móveis têm outras características e limitações que os separam de computadores tradicionais.
Toque na tela, variedade de telas e limitações de hardware foram todos considerados em Angular 2. Adicionalmente, o time de desenvolvimento do Framework de desenvolvimento mobile *Ionic*, estão trabalhando com Angular 2.
Os desktops também verá melhorias em desempenho e responsividade. Angular 2, assim como *React* e outros frameworks, pode alavancar ganhos de desempenho, renderizando HTML no servidor ou mesmo no navegador.

Abaixo algumas novidades presentes na nova versão do Framework:
 * Form Builder
 * Change Detection
 * Templating
 * Routing
 * Annotations
 * Observables
 * Shadow DOM

Devido a grande curva de aprendizado deste framework, alguns profissionais estão levando em conta migrar suas aplicações em AngularJs para outro novo framework, como por exemplo o *Arelia* que tem uma rota de migração mais simples.
Contudo existe outro grupo de profissionis que defendem o uso do Angular 2, afirmando existir uma infinidade de material disponivel, como livros e conteudos na internet que vão auxiliar e reduzir a curva de aprendizado, alem de ter um time da Google por traz do desenvolvimento e suporte.

#### Tecnologias relacionadas
 * npm
 * Node.js
 * TypeScript
 * CSS
 * bootstrap
 

#### Alo Mundo Angular 2
O [site oficial](https://angular.io/docs/ts/latest/quickstart.html#) do Angular oferece um QuickStart de uma aplicação que mostra uma simples mensagem.
Eu segui os passos do projeto quickstart a fim conhecer um pouco mais desta tecnologia, o resultado do passo a passo é a apresentação de um simpes mensagem na pagina inicial da aplicação.
Como pre-requisito de qualquer aplicação com o Framework Angular 2, é necessario ter instalado o *Node.js* e o *npm*,
e apartir do comando abaixo istalar as dependencias necessarias (as dependencias que serão baixadas são especificas pelo arquivo "package.json").
###### Instalar dependencias
`npm install`

Aplicações em Angular 2 são modulares, onde é possivel importar apenas as partes necessarias tornando assim a aplicação mais leve. Toda aplicação tem pelo menos um modulo, ou modulo principal.
Os modulos são responsaveis por controlar e gerenciar os componentes, um componete é responsavel por controlar uma parte do que é mostrado na tela (view) apartir de templates.
O HelloWorld presente neste repositório, pode ser executado com o seguinte comando pelo terminal:
###### Run
`npm start`


#### Caso real de uso da tecnologia
O site [Built With Angular 2](http://builtwithangular2.com/) traz exemplos open source de aplicações feitas utilizando o framework Angula 2. 

#### Livros e sítios para aprendizado
O [site oficial](https://angular.io/) do framework Angular 2 traz informações sobre novos releases, bug fixes, entre outras como tutorias/projetos chamados de ***live example***,
onde o ambiente de desenvolvimento já se encontra disponível para teste e alterações através de qualquer navegador.

O livro [ng-book 2](https://www.ng-book.com/2/) traz tudo que você precisa saber para trabalhar com Angular 2: exemplos, melhores praticas, testes...

O livro [Angular 2 Development with TypeScript](https://www.manning.com/books/angular-2-development-with-typescript) ensina como usar a linguagem TypeScript e como tirar vantagem de seus beneficios, e por fim como fazer *deploy* e testar uma aplicação Angula 2.

O livro [Angular 2 na prática](https://leanpub.com/livro-angular2) exibe todos os passos necessários para a construção de aplicações SPA utilizando Angular 2.